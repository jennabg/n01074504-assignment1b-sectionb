﻿//HAD TO CHANGE THIS, ADD ALL OF THESE TO THE TOP OF EVERY CLASS ***********************************************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//----------------------------------------------------------------------------------------

namespace HTTP5101BJennaGreenbergAssignment1b
{
    public class Delivery
    {
        // Data we want to store is Day of the week for box to be delievered and any special instructions

        public string dayofweek;
        public string instructions;

        public Delivery(string dow, string i)
        {
            dayofweek = dow;
            instructions = i;
        }
    }
}
