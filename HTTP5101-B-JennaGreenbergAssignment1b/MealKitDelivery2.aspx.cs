﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTTP5101BJennaGreenbergAssignment1b
{

    public partial class MealKitDelivery2 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e) { }




        

       protected void Order(object sender, EventArgs E)
        {
            if (!Page.IsValid)
            {
                return;
            }
            //// USED THE INSTRUCTOR CODE AS THE BASIS FOR WHAT'S BELOW, FOLLOWED THE LOGIC PRESENTED IN THE EXAMPLE FILE

            /*Here are my variable for my customer class
             */
            string name = customername.Text.ToString();
            string address = customeraddress.Text.ToString();
            string email = customeremail.Text.ToString(); 
            Customer newCustomer = new Customer();
            newCustomer.CustomerName = name;
            newCustomer.CustomerAddress = address;
            newCustomer.CustomerEmail = email;

            /*Here are my variable for my box class
             */

            List<string> preferences = new List<string>{};
            int boxsize = int.Parse(peopletofeed.Text);

            string type = diettype.Text.ToString(); 
            string allergies = customerallergies.Text.ToString();
            Box newbox = new Box(preferences, boxsize, type, allergies);

            //Converting the preferences to a list, tried to follow instructor code


           
            List<string> mypreferencesList = new List<string>();



            foreach (Control control in mypreferences.Controls)
            {
                if(control.GetType() == typeof(CheckBox))
                {
                    CheckBox preference = (CheckBox)control; 
                    if (preference.Checked)
                    {
                        mypreferencesList.Add(preference.Text); 
                    }
                }
            }
            newbox.preferences = newbox.preferences.Concat(mypreferencesList).ToList(); 

            /*Here are my variable for my Delivery class 
             */
            string dayofweek = deliverydate.SelectedItem.Text.ToString();
            string instructions = deliveryinstructions.Text.ToString();

            Delivery customerdelivery = new Delivery(dayofweek, instructions);




            Order customerorder = new Order(newCustomer, newbox, customerdelivery);
            details.InnerHtml = customerorder.PrintSummary();

        }
    }
} 
