﻿
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace HTTP5101BJennaGreenbergAssignment1b
{
    public class Customer
    {
        //Here we store the data we want for the customer class, this includes the customer name
        // customer address and customer email. This is based of instructor example.

        private string customername;
        private string customeraddress;
        private string customeremail;


        public Customer()
        {

        }
        public string CustomerName
            {

            get { return customername; }
            set { customername = value; }


            }
        public string CustomerAddress
        {
            get { return customeraddress; }
            set { customeraddress = value; }

        }
        public string CustomerEmail
        {
            get { return customeremail; }
            set { customeremail = value; }
        }
   
        
    }
}
