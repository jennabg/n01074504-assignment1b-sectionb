﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace HTTP5101BJennaGreenbergAssignment1b
{
    public class Box
    {
        // Info to be stored about the delivery box is the box size (how many people it will feed) , 
        //the box type and the preferences.


        public List<string> preferences;
        public int boxsize;
        public string type;
        public string allergies;

        
        public Box(List<string> p, int bs, string t, string a)
        {
            preferences = p;
            boxsize = bs;
            type = t;
            allergies = a;
        }
    }
}
