﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MealKitDelivery2.aspx.cs" Inherits="HTTP5101BJennaGreenbergAssignment1b.MealKitDelivery2" %>


<!DOCTYPE html>
<html>
<head runat="server">
	<title>MealKitDelivery2</title>
</head>
<body>
     <p id="newtest5" runat="server"></p>
	<form id="form1" runat="server">
            <p>What kinds of things do you look for in a meal? Check all items that apply.</p>
           <div id="mypreferences" runat="server">
                <asp:CheckBox runat="server" id="preferenceeasy" Text="Easy" />
                <asp:CheckBox runat="server" id="preferencehearty" Text="Hearty"/>
                <asp:CheckBox runat="server" id="preferencehealthy" Text="Healthy" />
                <asp:CheckBox runat="server" id="preferencelight" Text="Light" />
                <asp:CheckBox runat="server" id="preferenceimpressive" Text="impressive"/>
         
            </div>
            <div>
                <p> Dietary Preference </p>
               
               
             <asp:RadioButtonList runat="server" id="diettype">
               <asp:ListItem Text="Carnivore" Value="Carnivore" id="Carnivore" >Carnivore</asp:ListItem>
               <asp:ListItem Text="Pescetarian" Value="Pescetarian" id="Pescetarian" >Pescetarian</asp:ListItem>
               <asp:ListItem Text="Vegetarian" Value="Vegetarian" id="Vegetarian" >Vegetarian</asp:ListItem>
           </asp:RadioButtonList>
                
                
            </div> 
            <br />
            <asp:TextBox runat="server" id="customerallergies" placeholder="Any Allergies?"></asp:TextBox>
            <div>
                <p> How many people will your kit be feeding? (Please note max amount is 4.)</p>
                <asp:TextBox runat="server" id="peopletofeed" placeholder="# of people"></asp:TextBox>
                <asp:RangeValidator runat="server" ControlToValidate="peopletofeed" Type="Integer" MinimumValue="1" MaximumValue="4" ErrorMessage="Please enter a number between 1 and 4."></asp:RangeValidator>
            </div>
            <div>
                <p> Delivery Day Preference </p>
                <asp:DropDownList runat="server" id="deliverydate">
                    <asp:ListItem Value="M" Text="Monday"></asp:ListItem>
                    <asp:ListItem Value="T" Text="Tuesday"></asp:ListItem>
                    <asp:ListItem Value="W" Text="Wednesday"></asp:ListItem>
                    <asp:ListItem Value="TH" Text="Thursday"></asp:ListItem>
                    <asp:ListItem Value="F" Text="Friday"></asp:ListItem>
                    </asp:DropDownList>
            </div>
             <asp:TextBox runat="server" id="deliveryinstructions" placeholder="Special Delivery Instructions"></asp:TextBox>
            <div>
                <p> Contact Information </p>
                
               <asp:TextBox runat="server" id="customername" placeholder="Full Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="customername" ErrorMessage="Please enter your full name."></asp:RequiredFieldValidator>
              
                <asp:TextBox runat="server" id="customeraddress" placeholder="Customer Address"></asp:TextBox> 
                <asp:RequiredFieldValidator runat="server" ControlToValidate="customeraddress" ErrorMessage="Please enter your address."></asp:RequiredFieldValidator>
                <p> Email: <asp:TextBox runat="server" id="customeremail"></asp:TextBox></p>
                <asp:RegularExpressionValidator ID="emailvaildate" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="customeremail" ErrorMessage="Please enter a valid email"></asp:RegularExpressionValidator>
            </div>
            </ br>
            <asp:Button runat="server" id="signup" OnClick="Order" Text="Sign Up!"/>
            
            <asp:ValidationSummary id="valSum" DisplayMode="BulletList" runat="server" />
            
           <div runat="server" id="details">


           </div>
	</form>
</body>
</html>
