﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace HTTP5101BJennaGreenbergAssignment1b
{
    public class Order
    //Here is my Order class - I have my newcustomer object, my newbox object and my customerdelivery
    //object all inside. This is modeled off the instructor example from class.
    {
        public Delivery delivery;
        public Customer customer;
        public Box box;

        public Order(Customer newCustomer, Box newbox, Delivery customerdelivery)
        {
            delivery = customerdelivery;
            customer = newCustomer;
            box = newbox;
        }

        public string PrintSummary()
        { 
            string summary = "Here is a summary of your order: " + "<br/>";
            summary += "You chose a " + box.type + " box to feed " + box.boxsize + " people. "+"<br/>";
            summary += "Your book will be delivered on " + delivery.dayofweek + "." + "<br/>";
            summary += "Your delivery instructions are: " + delivery.instructions + "<br/>";
            summary += "The total cost for your box is: " + OrderTotal().ToString();

            return summary;

        }
        //Here is my method to calculate my order total.
        public double OrderTotal()
        {
            double sizetotal = 0;

            if (box.boxsize == 1)
            {
                sizetotal = 10;
            }
            else if (box.boxsize == 2)
            {
                sizetotal = 15;
            }
            else if (box.boxsize == 3)
            {
                sizetotal = 20;
            }
            else if (box.boxsize == 4)
            {
                sizetotal = 25;
            }

            double typetotal = 0;

            if (box.type == "Vegetarian")
            {
                typetotal = 5;
            }
            else if (box.type == "Pescetarian")
            {
                typetotal = 10;
            }
            else if (box.type == "Carnivore")
            {
                typetotal = 15;
            }

            double ordertotal = typetotal + sizetotal;

            return ordertotal;

        }


    }

}

